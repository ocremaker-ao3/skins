# Branching skin

This AO3 work skin allows you to create interactive branching text options depending on the reader's choices.
This tutorial assumes you have at least acquired some basic HTML knowledge, including on tags, classes and link elements.


## Example

```html
<p>
  <a name="choice-1-1+"></a><a name="choice-1-2+"></a><a name="choice-1-3+"></a><a name="choice-2-1+"></a><a name="choice-2-2+"></a>
  Make a choice! <a href="#choice-1-1+">Choice 1</a> / <a href="#choice-1-2+">Choice 2</a> / <a href="#choice-1-3+">Choice 3</a><br>
  <span class="branch-1 of-choice-1">This is the first dialog branch.</span>
  <span class="branch-2 of-choice-1 show-for-choice-2 valid">This is the second dialog choice.
  On multiple lines!
  With a breakline!<br><br>
  And it's time to make a second choice! <a href="#choice-2-1+">Choice 1</a> / <a href="#choice-2-2+">Choice 2</a></span>
  <span class="branch-3 of-choice-1">This is the third dialog branch.</span><br>
  <span class="branch-1 of-choice-2 valid">This is the second choice's first dialog branch.</span>
  <span class="branch-2 of-choice-2">This is the second choice's second dialog branch.</span><br>
</p>
```
([Show full example](https://gitlab.com/ocremaker-ao3/skins/-/blob/main/examples/test.html))

## How to use

In this example, the reader is presented with three different choices. By default, all the span with the `branch-b of-choice-c` classes are hidden.
When the reader clicks on the first one (Choice 1), the text in the span with the class `branch-1 of-choice-1` will be shown.
When he clicks on the second, the text in the span of of '`branch-2 of-choice-1`' will be shown
When he clicks on the third...
If he clicks on the second one, another choice will appear before him, and the same processus affects it (exept it's `of-choice-2` instead of 1).

How to apply:

1. Create a 'choices' paragraph. ALL the branching texts should be within it sequencially (see example).
    - Yes, that can mean your entire work. Be sure to NEVER have two break lines in the middle of the choices section.
    - If you do, replace them with two break tags.
2. In that paragraph, add empty links with the names corresponding to each choice.
    - In the example here, our first choice has three options and the second two.
    - So, we have five a elements "choice-1-1", "choice-1-2", "choice1-3", "choice-2-1", "choice-2-2".
3. Replace the p tags of each subsequent paragraph with a span with classes.
    - The `branch-b of-choice-c` classes will correspond to each choice you've made.
    - The `show-for-choice-c` classes is to be added to for every subchoice in this element.
    - Otherwise, the element would become hidden the moment another choice is made.
    - **For the branching to apply, your first class *must* be a branch class.**
4. Add links. Use a tags with an `href="#choice-(choice number)-(branch number)"` for each button that redirects to a choice.
5. (optional) Add an a element at the start with the name 'disable-choices' and href='#disable-choices'.
    - Add a valid class for every canon choice, they are the ones that will be shown when disabling choices.

## F.A.Q

How many choices can I make?

- By default, you can make up to 30 choices and 30 branches per choice in this version.
- These numbers are configurable in `_config.scss`, although keep in mind that AO3 skins have a hard limit of ~1500 style lines.

I want a single section to appear on multiple choices! How do I do that?

- You can add several `branch-b` classes to one span element. They will appear when either of the branches is active.
- However, keep in mind that adding several `of-choice-c` will render the text visible for *any* given branch of *any* choice given, so it's better to keep only one per span.
- If your text is to be shown when another choice is being made, use the `show-for-choice-c` classes instead.

Help! My text doesn't appear when I click on the button! What do I do?

- First off, check if you've spelt every class exactly. If you did, make sure AO3 didn't mess your formatting.

I want to hide the choices after they're done so the user can't go back. How do I do that?

- First, wrap your choice elements in a span.
- If this is your first choice, add the `hide-after-first-choice` class to it.
- Otherwise, use the `show-for-choice-(previous choice)` class to hide it automatically.

Why is the HTML so ugly?

- It was the only way to to do this without breaking browser compatibility and AO3's auto formatter.


## Updating from v1.0

The breaking change from 1.0 lies in the branches class, which has become a little more verbose. `branch-(choice number)-(branch number)` has become `branch-(branch number) of-choice-(choice number)`. Of course, if you wanna use multiple branches of the same choice, you don't have to put `of-choice-(choice number)` multiple times.
