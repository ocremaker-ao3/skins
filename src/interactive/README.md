# Choices skin

This folder contains the two SCSS files used to create many interactive mecanics like branching choices, and states.

It also contains a set of themes to be used with choices. Select one by using the `theme-<theme>` class in a parent.

Available themes:
- Text based (default)
- Ace Attorney (ace-attorney)
- Danganronpa (dangaronpa)


## How to use

- [Choices Tutorial](./CHOICES.md)
- [States Tutorial](./STATES.md)

## Examples

- [Choices examples](https://gitlab.com/ocremaker-ao3/skins/-/blob/main/examples/test.html)
- [States examples](https://gitlab.com/ocremaker-ao3/skins/-/blob/main/examples/states.html)
