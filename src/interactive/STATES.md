# States skin

This AO3 work skin allows you to create toggleable states that can be activated through a link.

'States' are akin to cumulative toggles (or multiple boolean variable), which you can set or unset. They're meant to be used in combination with branching choices, although they can be used with them. Thus, this guide assumes you've already read the tutorial on branches and choices.

For instance, a state can be used to remember that a dialog was viewed, a piece of evidence registered, or that a certain point in the story was reached.

## A basic state

States are identified by number (with a maximum of 10 by default).

You don't 'register' a state per se, but rather, integrate them into your previously prepared choices sections.

For instance a choice section that goes as such&hellip;
```html
<a name="choice-1-1+"></a>
<a name="choice-1-2+"></a>
<a name="choice-1-3+"></a>
...
<span class="hide-after-first-choice">
    <a href="#choice-1-1+">&gt; Dialog 1</a><br>
    <a href="#choice-1-2+">&gt; Dialog 2</a><br>
    <a href="#choice-1-3+">&gt; Dialog 3</a><br>
</span>
...
<span class="branch-1 of-choice-1">
    First branch.<br>
    <a href="#">&lt; Return</a>
</span>
<span class="branch-2 of-choice-1">
    Second branch.<br>
    <a href="#">&lt; Return</a>
</span>
<span class="branch-3 of-choice-1">
    Third branch.<br>
    <a href="#">&lt; Return</a>
</span>
```

You can 'register' a state (1 in this case) that will remember if dialog 1 was seen.

So, in order to access it, we'll change the button that leads to the first branch from:
```html
<a href="#choice-1-1+">&gt; Dialog 1</a><br>
```
to
```html
<a href="#choice-1-1+s1+">&gt; Dialog 1</a><br>
```

Notice the `s1+` we've added at the end of the link? When the reader clicks on the dialog 1 option the first state will be enabled.    
Of course, it doesn't do much for us right now. But what if we wanted to show the dialog 3 option ONLY after dialog 1 was seen?   

Just add the `state-1` class to the third branch choice!

```html
<a name="+s1+"></a>
<a name="choice-1-1+s1+"></a>
<a name="choice-1-2+"></a>
<a name="choice-1-3+"></a>
...
<span class="hide-after-first-choice">
    <a href="#choice-1-1+s1+">&gt; Dialog 1</a><br>
    <a href="#choice-1-2+">&gt; Dialog 2</a><br>
    <a href="#choice-1-3+" class="state-1">&gt; Dialog 3</a><br>
</span>
...
<span class="branch-1 of-choice-1">
    First branch.<br>
    <a href="#+s1+">&lt; Return</a>
</span>
<span class="branch-2 of-choice-1">
    Second branch.<br>
    <a href="#">&lt; Return</a>
</span>
<span class="branch-3 of-choice-1">
    Third branch.<br>
    <a href="#+s1+">&lt; Return</a>
</span>
```

**Note**: Remember to add the states to the return buttons as well, just like in the example!

In this case, we disable state one after the dialog three was clicked, in case we want to use it for something else later.

## How about NO?

What if we wanted to HIDE the dialog 3 option after the reader saw dialog one instead?

It couldn't be anymore simple, just do the same thing, but replace `class="state-1"` by `class="not-state-1"`

## Is it really that simple?

Well... no. As you've probably seen by now, much like the choices, states are remember in the location's hash. So, if the user clicks a link that doesn't contain the states, they'll all disappear without a trace.

For instance, in our example, if the reader clicked the dialog 2 option, the skin would forget about the reader having already read dialog 1.

So how do we solve the issue? Simple! (In theory, at the very least.)

If you've read the danganronpa skin, you should already be aware of **divergences**. Basically, depending on the current states, we'll show two different links that leads to the second branch, like this:

```html
<a href="#choice-1-2+" class="not-state-1">&gt; Dialog 2</a><a href="#choice-1-2+s1+" class="state-1">&gt; Dialog 2</a><br>
```

See how it works? If the state-1 is NOT enabled, we'll show the first link. If it is, on the other hand, we'll show the second one.

```html
<a name="+s1+"></a>
<a name="choice-1-1+s1+"></a>
<a name="choice-1-2+"></a><a name="choice-1-2+s1+"></a>
<a name="choice-1-3+"></a>
...
<span class="hide-after-first-choice">
    <a href="#choice-1-1+s1+">&gt; Dialog 1</a><br>
    <a href="#choice-1-2+" class="not-state-1">&gt; Dialog 2</a><a href="#choice-1-2+s1+" class="state-1">&gt; Dialog 2</a><br>
    <a href="#choice-1-3+" class="state-1">&gt; Dialog 3</a><br>
</span>
...
<span class="branch-1 of-choice-1">
    First branch.<br>
    <a href="#+s1+">&lt; Return</a>
</span>
<span class="branch-2 of-choice-1">
    Second branch.<br>
    <a href="#" class="not-state-1">&lt; Return</a><a href="#+s1+" class="state-1">&gt; Return</a><br>
</span>
<span class="branch-3 of-choice-1">
    Third branch.<br>
    <a href="#+s1+">&lt; Return</a>
</span>
```

**Note**: Remember to update your states registration at the beginning too!

So far so good, but as you can see, it starts getting a little messy. This will only get worse as time goes on, so make sure to have those links either dynamically generated, or at least use them with parsimony.

## Double Trouble

Using one state is cool. At one time, it allows you to save one information on top of choices.   
But this isn't enough power. What if we wanted the user to be able to see dialog 3 only after seing dialog 1 AND 2?   
And I'm not talking about taking them down a linear path by hiding either dialog 1 or 2 by default. You can already do that with choices.

No, in here, the reader can see both dialogs in EITHER order, and we can stay sure the reader got to see BOTH.

Enter, stage left: a second state.

So, let's do the same thing we did with the first one, but on dialog 2 and vice versa:
```html
<a name="+s1+"></a><a name="+s2+"></a><a name="+s1+s2+"></a>
<a name="choice-1-1+s1+"></a><a name="choice-1-1+s1+s2+"></a>
<a name="choice-1-2+s2+"></a><a name="choice-1-2+s1+s2+"></a>
<a name="choice-1-3+"></a>
...
<span class="hide-after-first-choice">
    <a href="#choice-1-1+s1+" class="not-state-2">&gt; Dialog 1</a><a href="#choice-1-1+s1+s2+" class="state-2">&gt; Dialog 1</a><br>
    <a href="#choice-1-2+s2+" class="not-state-1">&gt; Dialog 2</a><a href="#choice-1-2+s1+s2+" class="state-1">&gt; Dialog 2</a><br>
    <a href="#choice-1-3+" class="state-1 state-2">&gt; Dialog 3</a><br>
</span>
...
<span class="branch-1 of-choice-1">
    First branch.<br>
    <a href="#+s1+" class="not-state-2">&lt; Return</a><a href="#+s1+s2+" class="state-2">&gt; Return</a><br>
</span>
<span class="branch-2 of-choice-1">
    Second branch.<br>
    <a href="#+s2+" class="not-state-1">&lt; Return</a><a href="#+s1+s2+" class="state-1">&gt; Return</a><br>
</span>
<span class="branch-3 of-choice-1">
    Third branch.<br>
    <a href="#+s1+s2+">&lt; Return</a>
</span>
```

So far, we've just applied the same thing as the prior steps in reverse:
- Added `s2+` on every link that leads to dialog 2 and the return buttons.
- Updated the states registery to reflect that change.
- Added the `state-2` class to the dialog 3 link.
- Added a divergence on the dialog 1 dialog links depending on the status of state 2.

## OR we could do something different

Now, what if we wanted dialog 3 to be shown if *either* dialog 1 OR 2 were read. In other words, using an OR operation on state 1 and 2 rather than a AND (yes that pun in the title was terrible).

Simply add the `any-of` class to the dialog 3 link, which will apply an OR operation on the various states.

## Expending

Let's take this one step further. What if we had **4** dialogs, and we wanted the fourth dialog to be shown only after the first *three* were read? Let's start from where we stopped:
```html
<a name="+s1+"></a><a name="+s2+"></a><a name="+s1+s2+"></a>
<a name="choice-1-1+s1+"></a><a name="choice-1-1+s1+s2+"></a>
<a name="choice-1-2+s2+"></a><a name="choice-1-2+s1+s2+"></a>
<a name="choice-1-3+"></a>
<a name="choice-1-4+"></a>
...
<span class="hide-after-first-choice">
    <a href="#choice-1-1+s1+" class="not-state-2">&gt; Dialog 1</a><a href="#choice-1-1+s1+s2+" class="state-2">&gt; Dialog 1</a><br>
    <a href="#choice-1-2+s2+" class="not-state-1">&gt; Dialog 2</a><a href="#choice-1-2+s1+s2+" class="state-1">&gt; Dialog 2</a><br>
    <a href="#choice-1-3+">&gt; Dialog 3</a><br>
    <a href="#choice-1-3+" class="state-1 state-2">&gt; Dialog 4</a><br>
</span>
...
<span class="branch-1 of-choice-1">
    First branch.<br>
    <a href="#+s1+" class="not-state-2">&lt; Return</a><a href="#+s1+s2+" class="state-2">&gt; Return</a><br>
</span>
<span class="branch-2 of-choice-1">
    Second branch.<br>
    <a href="#+s2+" class="not-state-1">&lt; Return</a><a href="#+s1+s2+" class="state-1">&gt; Return</a><br>
</span>
<span class="branch-3 of-choice-1">
    Third branch.<br>
    <a href="#">&lt; Return</a>
</span>
<span class="branch-4 of-choice-1">
    Third branch.<br>
    <a href="#+s1+s2+">&lt; Return</a>
</span>
```

Now, we'll add a third state to remember the third dialog being read. So, let's do the same thing we did last time
- Added `s3+` on every link that leads to dialog 3 and the return buttons.
- Updated the states registery to reflect that change.
- Added the `state-3` class to the dialog 4 link.

```html
<a name="+s1+"></a><a name="+s2+"></a><a name="+s3+"></a>
<a name="+s1+s2+"></a><a name="+s1+s3+"></a><a name="+s2+s3+"></a><a name="+s1+s2+s3+"></a>
<a name="choice-1-1+s1+"></a><a name="choice-1-1+s1+s2+"></a>
<a name="choice-1-2+s2+"></a><a name="choice-1-2+s1+s2+"></a>
<a name="choice-1-3+s3+"></a>
<a name="choice-1-4+"></a>
...
<span class="hide-after-first-choice">
    <a href="#choice-1-1+s1+" class="not-state-2">&gt; Dialog 1</a><a href="#choice-1-1+s1+s2+" class="state-2">&gt; Dialog 1</a><br>
    <a href="#choice-1-2+s2+" class="not-state-1">&gt; Dialog 2</a><a href="#choice-1-2+s1+s2+" class="state-1">&gt; Dialog 2</a><br>
    <a href="#choice-1-3+s3+">&gt; Dialog 3</a><br>
    <a href="#choice-1-3+" class="state-1 state-2 state-3">&gt; Dialog 4</a><br>
</span>
...
<span class="branch-1 of-choice-1">
    First branch.<br>
    <a href="#+s1+" class="not-state-2">&lt; Return</a><a href="#+s1+s2+" class="state-2">&gt; Return</a><br>
</span>
<span class="branch-2 of-choice-1">
    Second branch.<br>
    <a href="#+s2+" class="not-state-1">&lt; Return</a><a href="#+s1+s2+" class="state-1">&gt; Return</a><br>
</span>
<span class="branch-3 of-choice-1">
    Third branch.<br>
    <a href="#+s3+">&lt; Return</a>
</span>
<span class="branch-4 of-choice-1">
    Fourth branch.<br>
    <a href="#+s1+s2+s3+">&lt; Return</a>
</span>
```

Now, let's add a divergence on the dialog 1 AND 2 dialog links depending on the status of state 3.

```html
<a name="+s1+"></a><a name="+s2+"></a><a name="+s3+"></a>
<a name="+s1+s2+"></a><a name="+s1+s3+"></a><a name="+s2+s3+"></a><a name="+s1+s2+s3+"></a>
<a name="choice-1-1+s1+"></a><a name="choice-1-1+s1+s2+"></a><a name="choice-1-1+s1+s3+"></a><a name="choice-1-1+s1+s2+s3+"></a>
<a name="choice-1-2+s2+"></a><a name="choice-1-2+s1+s2+"></a><a name="choice-1-2+s2+s3+"></a><a name="choice-1-2+s1+s2+s3+"></a>
<a name="choice-1-3+s3+"></a><a name="choice-1-3+s1+s3+"></a><a name="choice-1-3+s2+s3+"></a><a name="choice-1-3+s1+s2+s3+"></a>
<a name="choice-1-4+"></a>
...
<span class="hide-after-first-choice">
    <a href="#choice-1-1+s1+" class="not-state-2 not-state-3">&gt; Dialog 1</a><a href="#choice-1-1+s1+s2+" class="state-2 not-state-3">&gt; Dialog 1</a><br>
    <a href="#choice-1-1+s1+s3+" class="state-3 not-state-2">&gt; Dialog 1</a><a href="#choice-1-1+s1+s2+s3+" class="state-2 state-3">&gt; Dialog 1</a><br>
    <a href="#choice-1-2+s2+" class="not-state-1 not-state-3">&gt; Dialog 2</a><a href="#choice-1-2+s1+s2+" class="state-1 not-state-3">&gt; Dialog 2</a><br>
    <a href="#choice-1-2+s2+s3+" class="state-3 not-state-1">&gt; Dialog 2</a><a href="#choice-1-2+s1+s2+s3+" class="state-1 state-3">&gt; Dialog 2</a><br>
    <a href="#choice-1-3+s3+" class="not-state-1 not-state-2">&gt; Dialog 3</a><a href="#choice-1-3+s1+s3+" class="state-1 not-state-2">&gt; Dialog 3</a><br>
    <a href="#choice-1-3+s2+s3+" class="state-2 not-state-1">&gt; Dialog 3</a><a href="#choice-1-3+s1+s2+s3+" class="state-1 state-2">&gt; Dialog 3</a><br>
    <a href="#choice-1-4+" class="state-1 state-2 state-3">&gt; Dialog 4</a><br>
</span>
...
<span class="branch-1 of-choice-1">
    First branch.<br>
    <a href="#+s1+" class="not-state-2 not-state-3">&lt; Return</a><a href="#+s1+s2+" class="state-2 not-state-3">&gt; Return</a><br>
    <a href="#+s1+s3+" class="state-3 not-state-2">&lt; Return</a><a href="#+s1+s2+s3+" class="state-2 state-3">&gt; Return</a><br>
</span>
<span class="branch-2 of-choice-1">
    Second branch.<br>
    <a href="#+s2+" class="not-state-1 not-state-3">&lt; Return</a><a href="#+s1+s2+" class="state-1 not-state-3">&gt; Return</a><br>
    <a href="#+s2+s3+" class="not-state-1 state-3">&lt; Return</a><a href="#+s1+s2+s3+" class="state-1 state-3">&gt; Return</a><br>
</span>
<span class="branch-3 of-choice-1">
    Third branch.<br>
    <a href="#+s3+" class="not-state-1 not-state-2">&lt; Return</a><a href="#+s1+s3+" class="state-1 not-state-2">&gt; Return</a><br>
    <a href="#+s2+s3+" class="not-state-1 state-2">&lt; Return</a><a href="#+s1+s2+s3+" class="state-1 state-2">&gt; Return</a><br>
</span>
<span class="branch-4 of-choice-1">
    Fourth branch.<br>
    <a href="#+s1+s2+s3+">&lt; Return</a>
</span>
```

Phew. Now that's becoming a mess. I've added additional breaklines for it to be more readable, but it's still a bit hard to figure out what's going on. As I said before, either have something to auto generate those, or use them in parsimony.

## Growing complexity

So let's take a situation slightly more complicated than the previous one. What if we had **4** dialogs, and we wanted dialog 4 to only be shown if the reader has either seen dialog 1 or 2, but must have also seen dialog 3?

Well, doing ANDs of of OR and ANDs operations is pretty easy: just put one condition inside the other. In the previous example, replace
```html
<a href="#choice-1-3+" class="state-1 state-2 state-3">&gt; Dialog 4</a><br>
```
with
```html
<span class="state-3"><a href="#choice-1-3+" class="any-of state-1 state-2">&gt; Dialog 4</a></span><br>
```

The `span` checks if the state 3 is set (third dialog viewed), while the link checks if either state 1 or 2 are set.

## (Optional) Showing a tick for viewed dialogs/path that lead to a state.

With states, it becomes easy for a reader to get lost. So, in order to help them find their path, it's a good idea to add a small tick after the already viewed dialog options. Using what we've already seen before, we can do that by adding a simple element:

```html
<span class="state-n"> ✓</span>
```

So, you would get something like:
```html
<a name="+s1+"></a><a name="+s2+"></a><a name="+s3+"></a>
<a name="+s1+s2+"></a><a name="+s1+s3+"></a><a name="+s2+s3+"></a><a name="+s1+s2+s3+"></a>
<a name="choice-1-1+s1+"></a><a name="choice-1-1+s1+s2+"></a><a name="choice-1-1+s1+s3+"></a><a name="choice-1-1+s1+s2+s3+"></a>
<a name="choice-1-2+s2+"></a><a name="choice-1-2+s1+s2+"></a><a name="choice-1-2+s2+s3+"></a><a name="choice-1-2+s1+s2+s3+"></a>
<a name="choice-1-3+s3+"></a><a name="choice-1-3+s1+s3+"></a><a name="choice-1-3+s2+s3+"></a><a name="choice-1-3+s1+s2+s3+"></a>
<a name="choice-1-4+"></a>
...
<span class="hide-after-first-choice">
    <a href="#choice-1-1+s1+" class="not-state-2 not-state-3">&gt; Dialog 1</a><a href="#choice-1-1+s1+s2+" class="state-2 not-state-3">&gt; Dialog 1</a><a href="#choice-1-1+s1+s3+" class="state-3 not-state-2">&gt; Dialog 1</a><a href="#choice-1-1+s1+s2+s3+" class="state-2 state-3">&gt; Dialog 1</a><span class="state-1"> ✓</span><br>
    <a href="#choice-1-2+s2+" class="not-state-1 not-state-3">&gt; Dialog 2</a><a href="#choice-1-2+s1+s2+" class="state-1 not-state-3">&gt; Dialog 2</a><a href="#choice-1-2+s2+s3+" class="state-3 not-state-1">&gt; Dialog 2</a><a href="#choice-1-2+s1+s2+s3+" class="state-1 state-3">&gt; Dialog 2</a><span class="state-2"> ✓</span><br>
    <a href="#choice-1-3+s3+" class="not-state-1 not-state-2">&gt; Dialog 3</a><a href="#choice-1-3+s1+s3+" class="state-1 not-state-2">&gt; Dialog 3</a><a href="#choice-1-3+s2+s3+" class="state-2 not-state-1">&gt; Dialog 3</a><a href="#choice-1-3+s1+s2+s3+" class="state-1 state-2">&gt; Dialog 3</a><span class="state-3"> ✓</span><br>
    <a href="#choice-1-4+" class="state-1 state-2 state-3">&gt; Dialog 4</a><br>
</span>
...
<span class="branch-1 of-choice-1">
    First branch.<br>
    <a href="#+s1+" class="not-state-2 not-state-3">&lt; Return</a><a href="#+s1+s2+" class="state-2 not-state-3">&gt; Return</a><br>
    <a href="#+s1+s3+" class="state-3 not-state-2">&lt; Return</a><a href="#+s1+s2+s3+" class="state-2 state-3">&gt; Return</a><br>
</span>
<span class="branch-2 of-choice-1">
    Second branch.<br>
    <a href="#+s2+" class="not-state-1 not-state-3">&lt; Return</a><a href="#+s1+s2+" class="state-1 not-state-3">&gt; Return</a><br>
    <a href="#+s2+s3+" class="not-state-1 state-3">&lt; Return</a><a href="#+s1+s2+s3+" class="state-1 state-3">&gt; Return</a><br>
</span>
<span class="branch-3 of-choice-1">
    Third branch.<br>
    <a href="#+s3+" class="not-state-1 not-state-2">&lt; Return</a><a href="#+s1+s3+" class="state-1 not-state-2">&gt; Return</a><br>
    <a href="#+s2+s3+" class="not-state-1 state-2">&lt; Return</a><a href="#+s1+s2+s3+" class="state-1 state-2">&gt; Return</a><br>
</span>
<span class="branch-4 of-choice-1">
    Fourth branch.<br>
    <a href="#+s1+s2+s3+">&lt; Return</a>
</span>
```

Full example can be found [examples/states.html](https://gitlab.com/ocremaker-ao3/skins/-/blob/main/examples/ww-chapter3.html)


## Summary:

| What to do                        | How to do it                                                                     |
|-----------------------------------|----------------------------------------------------------------------------------|
| Set the state variable            | Add or remove `+s<number>+` from the link's destination.                         |
| Detect when a state is active     | Add the class `state-<number>` to the element.                                   |
| Detect when a state is not active | Add the class `not-state-<number>` to the element.                               |
| Do a AND of the prior detections  | Add the `state-<number>` and the `not-state-<number>` classes to the element.    |
| Do a OR of the prior detections   | The same, but add the `any-of` class.                                            |
| AND several ANDs and ORs          | Use several cascaded AND or OR detections.                                       |
| OR several ANDs and ORs           | Copy the same dialogs and, and add each respective AND or OR detections to each. |
