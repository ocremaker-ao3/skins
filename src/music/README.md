# Music skin

This (relatively minor) music indicator skin allows your reader to know which music to play as well as present convenient links for them to play it.

It isn't mandatory for all services to be present, but the more options you provide, the better for your reader.

Supported services:
- Youtube
- Invidious (youtube frontend)

It also provides a setting to allow the reader to disable the music all together:
```html
<details class="setting disable-music"><summary>Disable music</summary></details>
```


## Template

```html
<span class="music">(Music Name) <a class="yt" title="Play on youtube" href="https://youtu.be/(youtube link)">▶</a> <a class="invidious" title="Play on invidious" href="https://redirect.invidious.io/watch?v=(youtube link)">▶</a></span>
```
## Example

```html
<span class="music">Trial (Phoenix Wright: Ace Attorney – Trials and Tribulations) <a class="yt" title="Play on youtube" href="https://youtu.be/hLL2NtRNjUc">▶</a> <a class="invidious" title="Play on invidious" href="https://redirect.invidious.io/watch?v=hLL2NtRNjUc">▶</a></span>
```
