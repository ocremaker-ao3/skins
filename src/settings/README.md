# settings

Settings are used to provide checkable checkboxes at the start of a chapter (before the paragraph) to affect the rest of the fic.

Example setting:
```html
<details class="setting disable-music">
<summary>Disable music</summary>
</details>
```

Example slider:
```html
<p class="inline">Font size: </p>
<details class="slider font-size"><summary>1</summary></details>
<details class="slider font-size"><summary>2</summary></details>
<details class="slider font-size"><summary>3</summary></details>
<details class="slider font-size"><summary>4</summary></details>
<details class="slider font-size"><summary>5</summary></details>
<details class="slider font-size"><summary>6</summary></details>
<p class="last-label">7</p>
```
