# Formatting skin

This skin various text formatting styles I use in multiple fics. They can be set by adding it to an element's class list.

List of formattings (might contains spoilers if you haven't read all my fics):

| Class      | Description                                                                                                                      |
|------------|----------------------------------------------------------------------------------------------------------------------------------|
| center     | Creates a new block of text aligned in the center (works with span, instead of align which just works with p)                    |
| hr         | Creates a separating line to separate sections (works with span, instead of align which just works with p)                       |
| red        | For bloodcurling situations. Changes the text color to red.                                                                      |
| gray       | For tutorial situations. Changes the text color to gray.                                                                         |
| high-voice | For high volume, booming voices. Augments letter spacing and font size.                                                          |
| vampire    | For demonic voices. Sets the font to serif, augments letter spacing (less than high voice) and font size (more than high voice). |
| verdict    | For court verdicts. Same as vampire, but with much higher letter spacing, and the element's width is set to 100%.                |

