# Truth Bullet Obtained

Truth bullet obtained sections can be created within a single span in a paragraph, and thus can be put inside choices/non stop debate sections.

### 1. Creating the root node

First, make sure you find yourself within a paragraph (enclosed by `<p></p>` tags). Within said paragraph, create a span with the class `dr-tbo` like so:
```html
<p>
    <span class="dr-tbo">
    </span>
</p>
```

This span is going to be our root node for the entire section.

### 2. Creating the borders

A truth bullet section has two borders: one at the top, and one at the bottom. They are represented by spans with the class `dr-tbo-border`.

```html
<p>
    <span class="dr-tbo">
        <span class="dr-tbo-border"><span>TRUTH BULLET OBTAINED!</span></span><br>
        <span class="dr-tbo-border"></span><br>
    </span>
</p>
```

**Note**: The span on the top border surrounding the text is used to add a white background to text on the border.

For the sake of flexibility, the text within the top border can be set to anything you want. For example, if you want the section to be an update for a truth bullet, just change the text to 'TRUTH BULLET UPDATED'. Similarly, you can set a text on the bottom border as well by creating a span with text in it, instead of leaving it empty like we did in the example.

### 3. Adding the truth bullet itself

Now that we've reacher the heart of the matter, let's create the truth bullet itself. In between the two borders, create another span with the class `dr-large-bullet`.

Within this span, add a 325x65 image which will serve as the bullet background. In all of my fics, I used [this image](https://ocremaker-ao3.gitlab.io/images/dr/bullet/large.png) as background, but you can set any background you want.

After the image, create another span with your truth bullet name within it.

```html
<p>
    <span class="dr-tbo">
        <span class="dr-tbo-border"><span>TRUTH BULLET OBTAINED!</span></span><br>
        <span class="dr-large-bullet"><img src="https://ocremaker-ao3.gitlab.io/images/dr/bullet/large.png"><span>Burnt Gloves</span></span><br>
        <span class="dr-tbo-border"></span><br>
    </span>
</p>
```

Don't forget to add the `<br>` at the end of each line! Padding is added automatically for the borders and the large bullet.

### 4. Adding the bullet's description

Just after the bullet, add a few lines of text to describe the truth bullet:

```html
<p>
    <span class="dr-tbo">
        <span class="dr-tbo-border"><span>TRUTH BULLET OBTAINED!</span></span><br>
        <span class="dr-large-bullet"><img src="https://ocremaker-ao3.gitlab.io/images/dr/bullet/large.png"><span>Burnt Gloves</span></span><br>
        Found in a trash can near the crime scene, this pair of gloves has been burnt to the crisp. They’re still warm, meaning their they have been thrown recently. They most likely belonged to the murderer.<br><br>
        <span class="dr-tbo-border"></span><br>
    </span>
</p>
```

Add a `<br>` if you want a break line in the middle, but just like in Non Stop Debates, do **NOT** add *two* break lines, or AO3 will split the paragraph into two, and break the style with it.

### 5. (Optional) Add an examine button

Many detective style video games with an eivdence-like system allow for evidence to be examined from a finer angle. You can add a button to link to an external resource that shows the evidence in greater details (a photo, a 3D model...)

In order to add such a button, create a new link with the class `dr-tbo-examine` just after the description.

```html
<p>
    <span class="dr-tbo">
        <span class="dr-tbo-border"><span>TRUTH BULLET OBTAINED!</span></span><br>
        <span class="dr-large-bullet"><img src="https://ocremaker-ao3.gitlab.io/images/dr/bullet/large.png"><span>Burnt Gloves</span></span><br>
        Found in a trash can near the crime scene, this pair of gloves has been burnt to the crisp. They’re still warm, meaning their they have been thrown recently. They most likely belonged to the murderer.<br><br>
        <a class="dr-tbo-examine" href="(link)">EXAMINE</a>
    </span>
</p>
```

Congratulations! You've now created your first truth bullet obtained section!

Remember that if you use it in combination with a debate system, add a way for the readers to check the bullets description (either by adding a span just after a truth bullet in a non stop debate, or by recalling them at the beginning/end of a chapter).
        

## Examples

- [Example Truth Bullet Obtained Section](https://gitlab.com/ocremaker-ao3/skins/-/blob/main/examples/dr-tbo-example.html)
