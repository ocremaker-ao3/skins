# Non Stop Debates

Since creating those sections requires the use of the `choices` skin, you should take a look at the [`choices` documentation](../choices) before starting this one.

### 1. Create an empty paragraph

Like in the choices skin, choices only work within the same paragaph tags. If you want to make several paragraphs within it, replace the `p` tags with two line breaks at the end and span tags for separation.

```html
<p>
</p>
```

### 2. Registering choices states

Assuming you've already written your Non Stop Debate in text format beforehand, let's assume you already know how many truth bullets and statements you have.

- For each truth bullet, create an empty link with a name `choice-1-(truth bullet number starting from 1)`. Remember! You can only have <b>up to eight truth bullets at once</b> per debate.

- Similarly, for every statement you can agree/refute, create an empty link with the name `choice-2-(number of statement)`. Create one more link for the correct combination of truth bullet and statement. Much like the truth bullets, you can only have up to <b>up to eight statement branches at once</b>.

**Note**: If you want you to use more than eight statements in your debates, you can create a generic branch for several statements, and then use the same href down the line to refer to that branch.

```html
<p>
    <a name="choice-1-1+"></a><a name="choice-1-2+"></a><a name="choice-1-3+"></a><a name="choice-1-4+"></a><a name="choice-2-1+"></a><a name="choice-2-2+"></a><a name="choice-2-3+"></a><a name="choice-2-4+"></a><a name="choice-2-5+"></a><a name="choice-2-6+"></a><a name="choice-2-7+"></a><a name="choice-2-8+"></a>
</p>
```

In this case, for example, we've registered <b>4</b> truth bullets, and <b>7</b> statements (since the last one will be used for the combination of the correct truth bullet and correct statements).

### 3. Listing truth bullets

Once you've listed those states, we will truth bullets. For that, break a line after the states and add each bullet following this pattern:

```html
<a href="#choice-1-(bullet number)"><i class='dr-bullet'></i>(Truth Bullet Name)</a><br/>
```

**Note**: The `<i class='dr-bullet'></i>` is a shorthand to represent the bullet image before the truth bullet's name. You can remove it if you want, or replace it with something elese like a '&gt;' character (written `&gt;` in html).

```html
<p>
    <a name="choice-1-1+"></a><a name="choice-1-2+"></a><a name="choice-1-3+"></a><a name="choice-1-4+"></a><a name="choice-2-1+"></a><a name="choice-2-2+"></a><a name="choice-2-3+"></a><a name="choice-2-4+"></a><a name="choice-2-5+"></a><a name="choice-2-6+"></a><a name="choice-2-7+"></a><a name="choice-2-8+"></a>
    <a href="#choice-1-1+"><i class='dr-bullet'></i>Makoto’s Hair</a><br/>
    <a href="#choice-1-2+"><i class='dr-bullet'></i>Hoodie</a><br/>
    <a href="#choice-1-3+"><i class='dr-bullet'></i>Family Picture</a><br/>
    <a href="#choice-1-4+"><i class='dr-bullet'></i>Literally Nothing</a><br/><br/>
</p>
```

We've listed four truth bullets, **Makoto's Hair** as number 1, **Hoodie** as number 2, **Family Picture** as number 3, and **Literally Nothing** as number 4.

#### Using statements as truth bullet

In order to use statements as truth bullets, we can add hidden truth bullets that would only get shown when the reader picks the statement. So, to achieve that, we can create a new truth bullet where the `a` tag also has the `branch-b of-choice-c`.

Template:
```html
<a class="branch-(statement number) of-choice-1" href="#choice-1-(bullet number)"><i class='dr-bullet'></i>(Statement)</a>
```

**Note**: Do NOT use breaklines, otherwise, your later statement truth bullets will have a strange padding at their top.

Full example:
```html
<p>
    <a name="choice-1-1+"></a><a name="choice-1-2+"></a><a name="choice-1-3+"></a><a name="choice-1-4+"></a><a name="choice-2-1+"></a><a name="choice-2-2+"></a><a name="choice-2-3+"></a><a name="choice-2-4+"></a><a name="choice-2-5+"></a><a name="choice-2-6+"></a><a name="choice-2-7+"></a><a name="choice-2-8+"></a>
    <a href="#choice-1-1+"><i class='dr-bullet'></i>Makoto’s Hair</a><br/>
    <a href="#choice-1-2+"><i class='dr-bullet'></i>Hoodie</a><br/>
    <a href="#choice-1-3+"><i class='dr-bullet'></i>Family Picture</a><br/>
    <a href="#choice-1-4+"><i class='dr-bullet'></i>Literally Nothing</a><br/><br/>
    <a class="branch-11 of-choice-1" href="#choice-1-11"><i class='dr-bullet'></i>I think you’ve got the wrong person.</a><a class="branch-11 of-choice-1" href="#choice-1-11"><i class='dr-bullet'></i>The symbols prove you are the only one!</a><a class="branch-12 of-choice-1" href="#choice-1-12"><i class='dr-bullet'></i>It’s the spirit!</a>...
</p>
```

We'll cover how to access those truth bullets in the next section.

### 4. Adding the debate itself and the statements

Once more assuming that you've already written the text version of the debate, we'll add a new section for it. Let's start by creating a span shown for every selected bullet, and the last choice (the one that will be our correct one).

```html
<span class="branch-8 of-choice-2 show-for-choice-1">
</span>
```

In it, paste your HTML formatted text. Assuming you use **bolds** and <u>underlines</u> for highlighting statements, to recognize them, wrap the text *and its highlighting tags* under an `a` tag directing to `#choice-2-(branch the statement redirects to by default)`.

```html
<a href="#choice-2-1+"><u>I think you’ve got the wrong person.</u></a>
```

By this point, you should have something like this:

```html
<p>
    <a name="choice-1-1+"></a><a name="choice-1-2+"></a><a name="choice-1-3+"></a><a name="choice-1-4+"></a><a name="choice-2-1+"></a><a name="choice-2-2+"></a><a name="choice-2-3+"></a><a name="choice-2-4+"></a><a name="choice-2-5+"></a><a name="choice-2-6+"></a><a name="choice-2-7+"></a><a name="choice-2-8+"></a>
    <a href="#choice-1-1+"><i class='dr-bullet'></i>Makoto’s Hair</a><br/>
    <a href="#choice-1-2+"><i class='dr-bullet'></i>Hoodie</a><br/>
    <a href="#choice-1-3+"><i class='dr-bullet'></i>Family Picture</a><br/>
    <a href="#choice-1-4+"><i class='dr-bullet'></i>Literally Nothing</a><br/><br/>
    <span class="branch-8 of-choice-2 show-for-choice-1">“M-Me? A legendary hero? I-I’m sorry! <a href="#choice-2-1+"><u>I think you’ve got the wrong person.</u></a>”<br/><br/>
       “You bear the signs! The white shirt, the strand of hair… Just as the prophecy foretold! <a href="#choice-2-2+"><b>The symbols prove you are the only one!</b></a>” The all-too-enthusiastic nature of the Queen made Makoto finally understand that, unfortunately, the apple didn’t fall far from the tree.<br/><br/>
       “It’s not just the appearance, either. <a href="#choice-2-3+"><b>It’s the spirit!</b></a>” The king completes his wife with gusto. “You’ve braved the seas to come to here, to Novelsic!”<br/><br/>
       “B-B-But it was an accident! I swear <a href="#choice-2-4+"><u>I didn’t mean to come on that ship!</u></a>”<br/><br/>
       But the king’s eyes only shone brighter. “Divine Providence then? There is no better proof! <a href="#choice-2-5+"><b>Your presence here is the will of the Divine!</b></a> You were <i>destined</i> to be that hero! So all you have to do is… <a href="#choice-2-6+"><u>Embrace it</u></a>. The legendary spirit resides within you—!”<br/><br/>
       “<a href="#choice-2-7+"><b>NO THAT’S WRONG</b></a>!”<br/><br/>
       <i>Think you found the answer? Then click on the statement with the right bullet selected!</i><br/><br/>
   </span>
</p>
```

#### Picking statements as truth bullets

To make a statement pickable as a truth bullet, add a single link just *after* the statement with the class `dr-statement-picker`.

For example, add
```html
<a href="#choice-1-10" class="dr-statement-picker+"></a>
```
after
```html
<a href="#choice-2-1+"><u>I think you’ve got the wrong person.</u></a>
```
to create:
```html
“M-Me? A legendary hero? I-I’m sorry! <a href="#choice-2-1+"><u>I think you’ve got the wrong person.</u></a> <a href="#choice-1-10" class="dr-statement-picker+"></a>”<br/><br/>
```

### 5. Adding branches

Now that we have our truth bullet section, we want to have different texts being shown depending on which statement is chosen (how the correct one is reached will be explained later).

So, after the truth bullet section `span`, we will add other `span` sections for each branch. For instance, when we click on the first statement ("I think you've got the wrong person."):

```html
<span class="branch-1 of-choice-2">⸻⸻⸻ I think you’ve got the wrong person.<br/><br/>
    “No, we don’t!”<br/><br/> <!-- Remember! No double line breaks, or <p>s'! Use two <br/> instead! No <hr> either! Use <span class="hr"></span>: -->
    “Yes you do!”<br/><br/>
    “No we don’t!”<br/><br/>
    “YES YOU DO!”<br/><br/>
    “NO WE DON’T!”<br/><br/>
    ...<br/><br/>
    <a href="#choice-1-1+"><u>&lt; Return</u></a><br/><br/>
</span>
```

At the end of the paragraph, we've added a link to `#choice-1-1`. When clicked, it reselects the first truth bullet, whose list has been hidden by now. It's a good idea to have one at the end of every 'wrong' paragraph.

If you don't wish to create a section for each possible statement, you can create one 'generic' branch, and then set every `#choice-2-x` to being the same, and set the corresponding paragraph to your generic one.

By now, your truth bullet section is nearly complete! It should look akin to this:

```html
<p>
    <a name="choice-1-1+"></a><a name="choice-1-2+"></a><a name="choice-1-3+"></a><a name="choice-1-4+"></a><a name="choice-2-1+"></a><a name="choice-2-2+"></a><a name="choice-2-3+"></a><a name="choice-2-4+"></a><a name="choice-2-5+"></a><a name="choice-2-6+"></a><a name="choice-2-7+"></a><a name="choice-2-8+"></a>
    <a href="#choice-1-1+"><i class='dr-bullet'></i>Makoto’s Hair</a><br/>
    <a href="#choice-1-2+"><i class='dr-bullet'></i>Hoodie</a><br/>
    <a href="#choice-1-3+"><i class='dr-bullet'></i>Family Picture</a><br/>
    <a href="#choice-1-4+"><i class='dr-bullet'></i>Literally Nothing</a><br/><br/>
    <span class="branch-8 of-choice-2 show-for-choice-1">“M-Me? A legendary hero? I-I’m sorry! <a href="#choice-2-1+"><u>I think you’ve got the wrong person.</u></a>”<br/><br/>
       “You bear the signs! The white shirt, the strand of hair… Just as the prophecy foretold! <a href="#choice-2-2+"><b>The symbols prove you are the only one!</b></a>” The all-too-enthusiastic nature of the Queen made Makoto finally understand that, unfortunately, the apple didn’t fall far from the tree.<br/><br/>
       “It’s not just the appearance, either. <a href="#choice-2-3+"><b>It’s the spirit!</b></a>” The king completes his wife with gusto. “You’ve braved the seas to come to here, to Novelsic!”<br/><br/>
       “B-B-But it was an accident! I swear <a href="#choice-2-4+"><u>I didn’t mean to come on that ship!</u></a>”<br/><br/>
       But the king’s eyes only shone brighter. “Divine Providence then? There is no better proof! <a href="#choice-2-5+"><b>Your presence here is the will of the Divine!</b></a> You were <i>destined</i> to be that hero! So all you have to do is… <a href="#choice-2-6+"><u>Embrace it</u></a>. The legendary spirit resides within you—!”<br/><br/>
       “<a href="#choice-2-7+"><b>NO THAT’S WRONG</b></a>!”<br/><br/>
       <i>Think you found the answer? Then click on the statement with the right bullet selected!</i><br/><br/>
   </span><span class="branch-1 of-choice-2">⸻⸻⸻ I think you’ve got the wrong person.<br/><br/> <!-- First statement (note the branch-2-1, linked to the #choices-2-1 link) -->
       “No, we don’t!”<br/><br/>
       “Yes you do!”<br/><br/>
       “No we don’t!”<br/><br/>
       “YES YOU DO!”<br/><br/>
       “NO WE DON’T!”<br/><br/>
       ...<br/><br/>
       <a href="#choice-1-1+"><u>&lt; Return</u></a><br/><br/> <!-- The return statement reselects the first truth bullet, to return into the non stop debate -->
   </span><span class="branch-2 of-choice-2">⸻⸻⸻ The symbols prove you are the only one.<br/><br/> <!-- Second statement -->
       ...<br/><br/>
       <a href="#choice-1-1+"><u>&lt; Return</u></a><br/><br/>
   </span><span class="branch-3 of-choice-2">⸻⸻⸻ It’s the spirit!<br/><br/> <!-- Third statement -->
       ...<br/><br/>
       <a href="#choice-1-1+"><u>&lt; Return</u></a><br/><br/>
   </span><span class="branch-4 of-choice-2">⸻⸻⸻ I didn’t mean to come on that ship!<br/><br/> <!-- Fourth statement -->
       ...<br/><br/>
   </span><span class="branch-5 of-choice-2">⸻⸻⸻ Your presence here is the will of the Divine!<br/><br/></span>  <!-- Fifth statement statement -->
   </span><span class="branch-4 branch-5 of-choice-2"> <!-- Notice that this line has both the "branch-2-4" and "branch-2-5" classes, which means this paragraph will be shown for both the fourth and fifth statement -->
       ...<br/><br/>
       <a href="#choice-1-1+"><u>&lt; Return</u></a><br/><br/>
   </span><span class="branch-6 of-choice-2">⸻⸻⸻ Embrace it.<br/><br/> <!-- Sixth statement -->
       ...<br/><br/>
       <a href="#choice-1-1+"><u>&lt; Return</u></a><br/><br/>  <!-- Seventh statement -->
   </span><span class="branch-7 of-choice-2">⸻⸻⸻ NO, THAT’S WRONG!<br/><br/>
       ...<br/><br/>
       <a href="#choice-1-1+"><u>&lt; Return</u></a><br/><br/>
   </span><span class="branch-8 of-choice-2"><br/><span class="hr"></span><br/> 
       <!-- This is the Eighth branch. If you notice in the non stop debate, there is only one link that redirects to this branch, and it's only shown when the third evidence is selected.
       See the "branch-1-3" class on the a link, and the 1-1, 1-2, and 1-4 on the previous link, which links to the second paragraph.
       If this is where your fic continues, you can put it in this span. Remember! Make sure there is no <p> statement! -->
       “… before completing your math division course.”<br/><br/>
   </span>
</p>
```

### 6. Create a right answer

At the moment the truth bullet selected won't change a thing for the branches the statements redirect to. Obviously, we don't want that. We want to have a *correct* truth bullet (or perhaps several) thrown at one particular statement.

So, in order to accomplish that, we will need to create a small divergence in our text. For instance, let's take the second statement.

```html
<a href="#choice-2-2+"><b>The symbols prove you are the only one!</b></a>
```

By default, it redirects to the second branch. But we will want it to redirect to another branch (the eighth) when the third truth bullet (Family Picture) is selected. Now how do we do that? Well, in a similar manner of what we did with the branches of text, we will duplicate the statement, and add difference `branch` classes to it:

```html
<a href="#choice-2-2+" class="branch-1 branch-2 branch-4 of-choice-1 disable-animations"><b>The symbols prove you are the only one!</b></a><a href="#choice-2-8+" class="branch-3 of-choice-1 show-for-choice-2 disable-animations"><b>The symbols prove you are the only one!</b></a>
```

When the first, second, or fourth bullet are selected, the first link will be shown, and the statement will redirect to branch 2. When the third is selected, however, the second link will be shown, and the statement will redirect to branch 8.

Note the `disable-animations` class, which disable the opacity fade-in that happens when you select a choice. It's important so that the user won't see said fade-in on the evidence when switching selected bullet.

### 7. (Optional) Add the bullet smash animation

A 'bullet smash animation', is what similar to how it was made in the games. A 3D version of the truth bullet's name is shooted toward the statement and shatters it. It's a cool effect to have, but AO3 has limitations on the potential implementation, since it forbids the use of regular CSS 3D. So, in order to solve this issue, we have to cheat a little, by creating an image with said 3D effect already applied:

- First off, create an image in any kind of image editing software (like [GIMP](https://gimp.org)).
- Create a text object with the name of your correct truth bullet.
- Set the font to your any `sans-serif` font you want, its size to `62px` and its weight to maximum (generally `900`).
  - If your font doesn't support higher font weights, download one that allows it.
- Set its color to dark yellow (I use <font color='#d7a600'>#d7a600 █</font>).
- Give it a small black outline (in GIMP, use the drop shadow tool with a bit of groth radius and no blur).
- 3D Transform it with a Y (vertical) rotation of -45°
- Cut the image to only cover the text (in GIMP, simply copy the transformed layer onto a new image).
  - Your image height should be ~115px, and if it isn't, make sure to cut it to reach that height while keeping the text centered.


The end result should look similar to this:

<img src="https://ocremaker-ao3.gitlab.io/images/dr/truth-bullets/family-picture.png"/>

Now, in order to add the animation itself, upload it to an online image hosting service like [Imgur](https://imgur.com) or [Postimage](https://postimages.org/) (those are example, but any will do).

Then, to your correct statement, add the class `dr-valid-refutable`, and add an `img` tag with the direct link to the image you've uploaded as source, and `dr-truth-bullet` as class.

```html
<a href="#choice-2-8+" class="branch-3 of-choice-1 show-for-choice-2 disable-animations dr-valid-refutable"><b>The symbols prove you are the only one!</b><img class="dr-truth-bullet" src='https://ocremaker-ao3.gitlab.io/images/dr/truth-bullets/family-picture.png'></img></a>
```

**Note**: You should do some testing with the animation. Obviously, it's imperfect due to not really being 3D, and the CSS limitations makes it pretty hard for it to be centered in the statement. However, if your statement is short enough that the bullet hole lies out of the statement, you can add the `offset-plus-10` class to your `a`, which should put the bullet hole in a more acceptable position.

And there you go! You've created your first truth bullet section! If you want to chain Non Stop Debate, replace choice-1 by choice-3, and choice-2 by choice-4. You can do five of them until you reach the builtin limit of the `choices` skin.

Keep in mind, however, that if you want the previous text from the prior Non Stop Debates to keep being shown, you will need to: 1. keep it in the same paragraph tags, 2. put the show-for-choice-(numbers of new choices) class on all prior Non Stop Debate spans, 3. put the choices states of the newer debates with the first ones.

For example:

```html
<p>
    <a name="choice-1-1+"></a><a name="choice-1-2+"></a><a name="choice-1-3+"></a><a name="choice-1-4+"></a><a name="choice-2-1+"></a><a name="choice-2-2+"></a><a name="choice-2-3+"></a><a name="choice-2-4+"></a><a name="choice-2-5+"></a><a name="choice-2-6+"></a><a name="choice-2-7+"></a><a name="choice-2-8+"></a><a name="choice-3-1+"></a><a name="choice-3-2+"></a><a name="choice-3-3+"></a><a name="choice-3-4+"></a><a name="choice-4-1+"></a><a name="choice-4-2+"></a><a name="choice-4-3+"></a><a name="choice-4-4+"></a><a name="choice-4-5+"></a><a name="choice-4-6+"></a><a name="choice-4-7+"></a><a name="choice-4-8+"></a>
    <!--
    ...
    Non stop debate 1
    ..
    -->
    <span class="branch-8 of-choice-2 of-choice-4 show-for-choice-3 show-for-choice-5 show-for-choice-6 show-for-choice-7"> <!-- Add more show classes depending on the number of subsequent Non Stop Debates -->
        <a href="#choice-3-1+"><i class='dr-bullet'></i>Truth bullet 1</a><br/>
        <a href="#choice-3-2+"><i class='dr-bullet'></i>Truth bullet 2</a><br/>
        <a href="#choice-3-3+"><i class='dr-bullet'></i>Truth bullet 3</a><br/>
        <a href="#choice-3-4+"><i class='dr-bullet'></i>Truth bullet 4</a><br/><br/>
        <span class="show-for-choice-3 branch-4-8">
            <!--
            ...
            Non stop debate 2, but instead of choice-2-x, we use choice-4-x.
            ..
            -->
        </span>
    </span>
    <span class="branch-1 of-choice-4">...</span>
    <span class="branch-2 of-choice-4">...</span>
    <span class="branch-3 of-choice-4">...</span>
    <span class="branch-4 of-choice-4">...</span>
    <span class="branch-5 of-choice-4">...</span>
    <span class="branch-6 of-choice-4">...</span>
    <span class="branch-7 of-choice-4">...</span>
    <span class="branch-8 of-choice-4 of-choice-6 show-for-choice-5 show-for-choice-7">
        <!-- Rince and repeat if you want -->
    </span>
</p>
```


## Examples

- [Simplified example used in the tutorial from Whimsical Winds Chapter 3](https://gitlab.com/ocremaker-ao3/skins/-/blob/main/examples/ww-chapter3-simplified.html)
- [Actual Chapter 3](https://gitlab.com/ocremaker-ao3/skins/-/blob/main/examples/ww-chapter3.html)
