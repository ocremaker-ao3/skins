# Danganronpa debate skin

This folder contains the two SCSS files used to create many Danganronpa related mecanics like non stop debates, truth bullet obtained sections, and bullet smashing animations.


If you want to create a one, you can use this in combination with the `choices` skin.
It uses a combination of two 'choices' to create the effect.
In short, by default, you can only make up to 15 Non Stop Debates per chapter, with up to 30 truth bullets, and 29 statements per debate.

**Note**: Those numbers can be changed in `_config.scss` by changing the `choice-count` and `branch-count`.

## How to use

One important thing to note is that this skin disables underlines for choice links, and requires to use `<span class="hr"></span>` instead of `<hr>` for paragraph breaks in choices/truth bullet sections (otherwise, AO3's formatter would break your text down).

- [Non Stop Debate Tutorial](./NON_STOP_DEBATE.md)
- [Truth Bullet Obtained Tutorial](./TRUTH_BULLET_OBTAINED.md)

## Examples

- [Simplified example used in the tutorial from Whimsical Winds Chapter 3](https://gitlab.com/ocremaker-ao3/skins/-/blob/main/examples/ww-chapter3-simplified.html)
- [Actual Chapter 3](https://gitlab.com/ocremaker-ao3/skins/-/blob/main/examples/ww-chapter3.html)
