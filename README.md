# ocremaker's AO3 skins

This repository contains several skin components that can be bundled together depending on your requirements. Several skins have interactive features and external images (more details in the list of skins below), so I'm unable to just publish them to the public skin list (and the CSS itself, being compiled, is a bit ugly).

## Usage

Each skin posseses several styles that might not be obvious how to be used. A proper tutorial is provided in each skin's directory on how to use them.

## Testing

You can test those skins online at the [Online Tester](https://ocremaker-ao3.gitlab.io/choices-skin-tester/) (note: it does NOT include help on how to apply the skins to your fic).

## Configuration

Several parameteres can be configured ahead of build times (like the choices limits or animation durations) in `_config.scss`. When changing said parameters, it's important to remember that AO3 has a hardcoded limit of skin styles line count at about 1500.

## Building

The skins themselves are not written in CSS directly, but [Sass](https://sass-lang.org). You can compile it using the compiler on their website.

Use `sass src/all.scss build/all.css` to build all skins into one.

Like I said earlier AO3 has a limit on style line counts (~1500), so don't go bundling too many skins at once into one. You can select, or deselect which skins you want to bundle by commenting in or out components in `src/all.scss`.

Alternatively, you can grab yourself one of the prebuilt CSS skins from the [Deployments > Releases](https://gitlab.com/ocremaker-ao3/skins/-/releases) tab.

## List of skins

Global skins version: v2.2

| Skin name     | Description                                      | Interactive | Contains External Images | Directory          | Dependencies                   |
|---------------|--------------------------------------------------|-------------|--------------------------|--------------------|--------------------------------|
| Settings      | Provide simple to use checkboxes for settings    | Yes         | No                       | src/settings/      | None                           |
| Formatting    | Various formatting styles (red, high voice...)   | No          | No                       | src/formatting/    | Settings                       |
| Music         | Small informative music indicator                | No          | Yes                      | src/music/         | Settings                       |
| Themes        | Different stylings for interactive skins         | No          | No                       | src/themes/        | Settings                       |
| Interactive   | Base skin to add interactivity (choices, states) | Yes         | No                       | src/interactive/   | Themes                         |
| Danganronpa   | Skin helping to create truth bullet sections     | Yes         | Yes                      | src/danganronpa/   | Interactive, Formatting, Music |
| Ghost Trick   | Skin helping to create trick time sections       | Yes         | Yes                      | src/ghosttrick/    | Interactive, Formatting, Music |

## License

All of the skins are made by myself (ocremaker) and licensed under MIT (see the `LICENSE` file).
